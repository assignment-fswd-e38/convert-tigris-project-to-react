import React from 'react'
import './App.css'
import Tigrisheader from './assets/Components/header'
import Section1 from './assets/Components/section1'
import Section2 from './assets/Components/section2'
import Section3 from './assets/Components/section3'
function App() {


  return (
    <>
      <>
      <Tigrisheader />
      <Section1 />
      <Section2 />
      <Section3 />
      </>
    </>
  )
}

export default App
