function Section1(){
    return(
        <section>
        <div className="container">
          <h1>Preserving the Majesty of Tigers in Their Natural Habitat</h1>
          <p>Explore Tigris Reserve, a sanctuary dedicated to the conservation of majestic tigers in their natural habitat. Immerse yourself in the beauty of untamed wilderness, where our commitment to preservation ensures a thriving ecosystem. Join us in safeguarding these magnificent creatures for future generations to witness and cherish.</p>
          <div className="buttonholder">
            <a className="button buttonprimary" href="#">Book a Safari</a>
            <a className="button buttonsecondary" href="#">Contact Us</a> 
          </div>
        </div>           
      </section>
    )
}
export default Section1