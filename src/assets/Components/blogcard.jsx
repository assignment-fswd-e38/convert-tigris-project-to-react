import { useState } from "react"

function Blogcard(props){
console.log(props.currentblog)
const [likecount, setlikecount]=useState(0)
    function count(){
        setlikecount(likecount+1); 
    }
    return(
        <article>
            <div className="blog">
                <a href="#">
                    <img src={props.currentblog.image} alt=""></img>
                    <h3>{props.currentblog.title}</h3>
                    <p>{props.currentblog.description}</p>
                </a>
            </div>
                <button onClick={count} id="likebutton">
                <span id="thumbsup" class="material-symbols-outlined">
                    favorite
                </span>
                    <span id="like">{likecount}</span> 
                </button>
                        
        </article>
    )
}

export default Blogcard