import React from "react"
import Blogcard from "./blogcard"
import image1 from "./images/Tiger11.jpg"
import image2 from "./images/Tiger2.jpg"
import image3 from "./images/Tiger1.jpg"
import image4 from "./images/Tiger14.jpg"
import image5 from "./images/Tiger4.jpg"
import image6 from "./images/Tiger13.jpg"
import image7 from "./images/Tiger15.jpg"
import image8 from "./images/Tiger8.jpg"
import image9 from "./images/Tiger10.jpg"
import image10 from "./images/Tiger7.jpg"
import image11 from "./images/Tiger3.jpg"
import image12 from "./images/Tiger5.jpg"
import image13 from "./images/Tiger6.jpg"
import image14 from "./images/Tiger9.jpg"
import image15 from "./images/Tiger16.jpg"
import image16 from "./images/Tiger12.jpg"
import image17 from "./images/Tiger17.jpg"

function Section3(){
    
    const blogs =[
        {
            id: 1,
            image: image1,
            title: "Roar into knowledge",
            description: "Dive deep into tiger conservation with 'Roar into Knowledge,' exploring vital insights and discoveries about Tigris Reserve and its inhabitants."
        },
        {
            id: 2,
            image: image2,
            title: "Wild Chronicles",
            description: "Discover thrilling tiger tales and conservation updates in our 'Wild Chronicles' blog, your window into the wonders of Tigris Reserve."
        },
        {
            id: 3,
            image: image3,
            title: "Tiger Tracks",
            description: "Follow the pawprints of tigers in 'Tiger Tracks,' where we share updates, stories, and insights from the wilds of Tigris Reserve."
        },
        {
            id: 4,
            image: image4,
            title: "Jungle Journals",
            description: "Explore untamed adventures and conservation stories in 'Jungle Journals,' where we share the remarkable tales of Tigris Reserve's inhabitants."
        },
        {
            id: 5,
            image: image5,
            title: "Roar Updates",
            description: "Stay informed with 'Roar Updates,' your source for the latest news and developments in tiger conservation efforts at Tigris Reserve."
        },
        {
            id: 6,
            image: image6,
            title: "Tigris Tales",
            description: "Experience the magic of the jungle through 'Tigris Tales,' where captivating stories of wildlife adventures await, unfolding the mysteries of nature."
        },
        {
            id: 7,
            image: image7,
            title: "Safari Stories",
            description: "Dive into the heart of the wild with 'Safari Stories,' sharing thrilling adventures and encounters from Tigris Reserve expeditions."
        },
        {
            id: 8,
            image: image8,
            title: "Cat Chronicles",
            description: "Explore the wild through 'Cat Chronicles,' revealing the untamed adventures and conservation efforts within Tigris Reserve's feline domain."
        },
        {
            id: 9,
            image: image9,
            title: "Tiger Tributes",
            description: "Explore 'Tiger Tributes' for heartfelt stories honoring the beauty and significance of tigers in Tigris Reserve's ecosystem."
        },
        {
            id: 10,
            image: image10,
            title: "Reserve Reports",
            description: "Stay updated with our Reserve Reports, offering insights into tiger populations, conservation efforts, and habitat preservation at Tigris Reserve."
        },
        {
            id: 11,
            image: image11,
            title: "Feline Focus",
            description: "Feline Focus: Dive deep into the world of majestic tigers, exploring their behavior, habitats, and conservation efforts at Tigris Reserve."
        },
        {
            id: 12,
            image: image12,
            title: "Jungle Dispatches",
            description: "Stay informed with 'Jungle Dispatches,' where we share updates and insights on wildlife conservation efforts at Tigris Reserve."
        },
        {
            id: 13,
            image: image13,
            title: "Tigris Tidbits",
            description: "Dive into bite-sized updates on tiger conservation efforts and reserve happenings in our Tigris Tidbits blog series."
        },
        {
            id: 14,
            image: image14,
            title: "Wildlife Whispers",
            description: "Discover untold secrets of the wild in 'Wildlife Whispers,' where nature's whispers unveil the hidden wonders of Tigris Reserve."
        },
        {
            id: 15,
            image: image15,
            title: "Tiger Time",
            description: "Tiger Time blog: Dive into the world of tigers, exploring their behavior, habitat, and conservation efforts at Tigris Reserve."
        },
        {
            id: 16,
            image: image16,
            title: "Nature Narratives",
            description: "Explore the untamed with Nature Narratives, where wilderness tales from Tigris Reserve come to life in vivid detail."
        },
        {
            id: 17,
            image: image17,
            title: "Tiger Trails",
            description: "Embark on Tiger Trails: Explore the untamed wilderness and follow the majestic footsteps of these magnificent creatures."
        },
        
    ]

    const blogcards = blogs.map(blog =>
    <Blogcard key={blog.id} currentblog={blog} />
)
    return(
    <section>
            <div className="container">
                <h2>Roars into Knowledge</h2>
                <div className="blogslist">
                {blogcards}

                </div>
            </div>
        </section>
    )
}
export default Section3