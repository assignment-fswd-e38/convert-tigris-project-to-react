function Tigrisheader(){

    return(
      
        <header>
        <div className="container">
          <a href="#">
            <span id="logo">Tigris Reserve</span>
          </a>
          <button id="menubutton">
            <span className="material-symbols-outlined">
              pets
            </span>
          </button>
          <nav>
            <ul id="navlist">
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">About</a>
              </li>
              <li>
                <a href="#">Services</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    )
}
export default Tigrisheader