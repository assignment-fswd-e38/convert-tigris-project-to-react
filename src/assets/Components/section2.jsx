function Section2(){
    return(
        <section id="aboutsection">
        <div className="container">
          <h2>Discover Our Mission and Vision</h2>
          <p>At Tigris Reserve, our mission is clear: safeguarding the future of endangered tiger populations through conservation efforts and habitat protection. With a vision of fostering harmony between humans and wildlife, we strive to educate, inspire, and advocate for the preservation of these magnificent creatures and their ecosystems.</p>  
        </div>
      </section>
    )
}
export default Section2